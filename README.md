# Film finder
React App for exploring movies

# Description
App for explore films

# Install
Clone repo
```Shell
git clone https://iliyaking46@bitbucket.org/iliyaking46/film-finder.git
```

Install deps
```shell
npm install
```

Launch dev-server with command below
```Shell
npm start
```

# For production build
Launch `npm` with command `build` for production build (output directory `./dist`)
```Shell
npm run build
```