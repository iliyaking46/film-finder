import React, {useState} from 'react';
import {Input, Container, Header, Card} from 'semantic-ui-react'
import {searchMoviesApi} from '../api'

const ENTER = 'Enter'

export const SearchPage = () => {
    const [searchValue, setSearchValue] = useState('стражи')
    const [result, setResult] = useState({})
    const {films = []} = result;

    const getFilms = () => {
        searchMoviesApi(searchValue)
            .then(setResult)
    }

    const handleSearch = (e) => {
        if (e.key === ENTER) {
            getFilms()
        }
    }

    return (
        <Container style={{padding: '4em 0em'}}>
            <Header as='h2'>Search movies</Header>
            <Container text style={{paddingBottom: '2em'}}>
                <Input
                    fluid
                    action={{
                        icon: 'search',
                        onClick: getFilms
                    }}
                    placeholder='Search...'
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    onKeyPress={handleSearch}
                />
            </Container>
            {
                films.length > 0
                    ? (
                        <Card.Group>
                            {films.map(({filmId, nameRu, posterUrlPreview, description, genres}) => (
                                <Card
                                    style={{width: '270px'}}
                                    key={filmId}
                                    image={posterUrlPreview}
                                    header={nameRu}
                                    meta={genres.map(({genre}) => genre).reduce((acc, genre) => (acc + ', ' + genre))}
                                    description={description}
                                    href={`https://www.kinopoisk.ru/film/${filmId}/`}
                                    target='_blank'
                                // extra={extra}
                                />
                            ))}
                        </Card.Group>
                    )
                    : <div>Sorry, nothing was found</div>
            }

        </Container>
    )
}