import apiKey from '../../api-key.json'
const SEARCH_URL = 'https://kinopoiskapiunofficial.tech/api/v2.1/films/search-by-keyword';
const REQUEST_PARAMS = {
    headers: {
        'X-API-KEY': apiKey
    }
}

export const getMoviesApi = async (id) => {
    // write code here
    // https://kinopoiskapiunofficial.tech/documentation/api/#/
    return {}
}

export const searchMoviesApi = async (searchValue, page = 1) => {
    if (!searchValue) {
        return {}
    }

    const requestUrl = `${SEARCH_URL}?keyword=${searchValue}&page=${page}`

    try {
        const request = await fetch(requestUrl, REQUEST_PARAMS)
        const data = await request.json()
        return data
    } catch (e) {
        console.error(e)
    }
}
